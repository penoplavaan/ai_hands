package calc

import "math"

var vectors = [...][5]float64{
	{4.16, 5.42, 0.68, 4.86, 7.01},
    {0.84, 0.58, 3.52, 0.78, 3.75},
}

var labels = [...]string{
	"Люблю",
	"Нет",
}

func calc(vectorToBeDeterminated [5]float64) string {
	closestDistance := calculateDistance(vectorToBeDeterminated, vectors[0])
    closestIndex := 0

	for i := 1; i < len(vectors); i++ {
		currentDistance := calculateDistance(vectorToBeDeterminated, vectors[i])
		if currentDistance < closestDistance {
			closestDistance = currentDistance
            closestIndex = i
		}
	}

    return labels[closestIndex]
}

func calculateDistance(x [5]float64, y [5]float64) float64 {
	distance := math.Sqrt(
        math.Pow(x[0]-y[0], 2) +
        math.Pow(x[1]-y[1], 2) +
        math.Pow(x[2]-y[2], 2) +
        math.Pow(x[3]-y[3], 2) +
        math.Pow(x[4]-y[4], 2),
	)

	return distance
}
