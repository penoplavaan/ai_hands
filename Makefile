app:
	sudo docker-compose exec go bash

ps:
	docker-compose ps

build:
	go build -o bin/server cmd/main.go

d.up:
	sudo docker-compose up

d.up.d:
	sudo docker-compose up -d

d.down:
	sudo docker-compose down

d.up.build:
	docker-compose up -d --build