package main

import (
	"log"
	"math"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
)

func main() {
	app := fiber.New()

	app.Use("/ws", func(c *fiber.Ctx) error {
		// IsWebSocketUpgrade returns true if the client
		// requested upgrade to the WebSocket protocol.
		if websocket.IsWebSocketUpgrade(c) {
			c.Locals("allowed", true)
			return c.Next()
		}
		return fiber.ErrUpgradeRequired
	})

	app.Get("/ws/:id", websocket.New(func(c *websocket.Conn) {
		// c.Locals is added to the *websocket.Conn
		log.Println(c.Locals("allowed"))  // true
		log.Println(c.Params("id"))       // 123
		log.Println(c.Query("v"))         // 1.0
		log.Println(c.Cookies("session")) // ""

		// websocket.Conn bindings https://pkg.go.dev/github.com/fasthttp/websocket?tab=doc#pkg-index
		var (
			mt  int
			msg []byte
			err error
		)
		for {
			if mt, msg, err = c.ReadMessage(); err != nil {
				log.Println("readErr:", err)
				break
			}

			var stringifiedPayload string
			var stringifiedVector []string
			var vector [5]float64

			stringifiedPayload = string(msg[:])
			log.Println("stringifiedPayload:", stringifiedPayload)

			stringifiedVector = strings.Split(stringifiedPayload, ",")
			log.Println("stringifiedVector:", stringifiedVector)

			for i := 0; i < 5; i++ {
				num, err := strconv.ParseFloat(stringifiedVector[i], 64)
				if err == nil {
					vector[i] = num
				} else {
					log.Println("error:", err)
				}
			}

			log.Println("vector:", vector)
			distance := calc(vector)
			log.Println("distance:", distance)

			if err = c.WriteMessage(mt, []byte(distance)); err != nil {
				log.Println("writeErr:", err)
				break
			}
		}
	}))

	log.Fatal(app.Listen(":9876"))
	// Access the websocket server: ws://localhost:3000/ws/123?v=1.0
	// https://www.websocket.org/echo.html
}

var vectors = [...][5]float64{
	{4.16, 5.42, 0.68, 4.86, 7.01},
	{0.84, 0.58, 3.52, 0.78, 3.75},
    {1.052363156730163, 0.7419418641454046, 1.0770510781958844, 0.7840719388621159, 3.0353589076746186},
    {0.9256918940770748, 0.8572522844248172, 1.0906010561545127, 0.8040856915834982, 2.894137033905171},
    {5.0021143776886134, 1.0156944247345598, 1.1429285351667098, 1.6584117393707678, 4.5582307954053585},
    {5.08189345273407, 1.338956079102115, 0.8609671227336952, 1.5561142931205587, 6.917592258999121},
    {5.083101360034805, 0.9989710982114152, 0.7194694973583993, 1.2908999621066832, 6.360685195527587},
}

var labels = [...]string{
	"Люблю",
	"Нет",
    "Да",
    "Извини",
		"Привет",
		"Пожалуйста",
		"Спасибо",
}

func calc(vectorToBeDeterminated [5]float64) string {
	closestDistance := calculateDistance(vectorToBeDeterminated, vectors[0])
	log.Println("closestDistance:", closestDistance)
	closestIndex := 0
	log.Println("closestIndex:", closestIndex)

	for i := 1; i < len(vectors); i++ {
		currentDistance := calculateDistance(vectorToBeDeterminated, vectors[i])
		if currentDistance < closestDistance {
			closestDistance = currentDistance
			closestIndex = i
		}
	}

	return labels[closestIndex]
}

func calculateDistance(x [5]float64, y [5]float64) float64 {
	distance := math.Sqrt(
		math.Pow(x[0]-y[0], 2) +
			math.Pow(x[1]-y[1], 2) +
			math.Pow(x[2]-y[2], 2) +
			math.Pow(x[3]-y[3], 2) +
			math.Pow(x[4]-y[4], 2),
	)

	return distance
}
