FROM golang:latest
WORKDIR /go/src/ai-hands
COPY . .
RUN go build -o bin/server main.go
CMD ["./bin/server"]