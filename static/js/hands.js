const video3 = document.getElementsByClassName('input_video3')[0];
const out3 = document.getElementsByClassName('output3')[0];
const controlsElement3 = document.getElementsByClassName('control3')[0];
const canvasCtx3 = out3.getContext('2d');
const fpsControl = new FPS();
const json = document.getElementById('json');

let distances = [];

const spinner = document.querySelector('.loading');
const wsConnection = new WebSocket("ws://localhost:4000/ws/1/?v=1.0");
wsConnection.onopen = function() {
    console.log("Соединение установлено.");
};
 wsConnection.addEventListener('message', function (event) {
     json.innerHTML = '<h2>'+event.data+'</h2>'
    console.log('Message from server', event.data);
});
spinner.ontransitionend = () => {
    spinner.style.display = 'none';
};

function onResultsHands(results) {
    document.body.classList.add('loaded');
    fpsControl.tick();


    canvasCtx3.save();
    canvasCtx3.clearRect(0, 0, out3.width, out3.height);
    canvasCtx3.drawImage(
        results.image, 0, 0, out3.width, out3.height);

    if (results.multiHandLandmarks && results.multiHandedness) {
        for (let index = 0; index < results.multiHandLandmarks.length; index++) {
            const classification = results.multiHandedness[index];
            const isRightHand = classification.label === 'Right';
            const landmarks = results.multiHandLandmarks[index];
            drawConnectors(
                canvasCtx3, landmarks, HAND_CONNECTIONS,
                {color: isRightHand ? '#00FF00' : '#FF0000'}),
                drawLandmarks(canvasCtx3, landmarks, {
                    color: isRightHand ? '#00FF00' : '#FF0000',
                    fillColor: isRightHand ? '#FF0000' : '#00FF00',
                    radius: (x) => {
                        return lerp(10, -0.15, .1, 10, 1);
                    }
                });

            distance = calculateDistancesBetweenFingertips(results.multiHandLandmarks[0])
									console.log(distance)
//            seconds = (new Date()).getSeconds();
//
            wsConnection.send(distance);
//
//            if (seconds % 2 == 0) {
//                distances.push(distance)
//                if (distances.length == 30) {
//                    result = getAvgDistance(distances)
//                    wsConnection.send(result);
//
//                    console.log(result)
//                }
//            } else {
//                distances = []
//            }
        }
    }
    canvasCtx3.restore();
}

const hands = new Hands({
    locateFile: (file) => {
        return `https://cdn.jsdelivr.net/npm/@mediapipe/hands@0.1/${file}`;
    }
});
hands.onResults(onResultsHands);

const camera = new Camera(video3, {
    onFrame: async () => {
        await hands.send({image: video3});
    },
    width: 480,
    height: 480
});
camera.start();

new ControlPanel(controlsElement3, {
    selfieMode: true,
    maxNumHands: 1,
    minDetectionConfidence: 0.5,
    minTrackingConfidence: 0.5
})
    .add([
        new StaticText({title: 'MediaPipe Hands'}),
        fpsControl,
        new Toggle({title: 'Selfie Mode', field: 'selfieMode'}),
        new Slider(
            {title: 'Max Number of Hands', field: 'maxNumHands', range: [1, 4], step: 1}),
        new Slider({
            title: 'Min Detection Confidence',
            field: 'minDetectionConfidence',
            range: [0, 1],
            step: 0.01
        }),
        new Slider({
            title: 'Min Tracking Confidence',
            field: 'minTrackingConfidence',
            range: [0, 1],
            step: 0.01
        }),
    ])
    .on(options => {
        video3.classList.toggle('selfie', options.selfieMode);
        hands.setOptions(options);
    });


function getDistance(object1, object2) {
    return Math.sqrt(
        (Math.pow((object1.x - object2.x), 2))
        + (Math.pow((object1.y - object2.y), 2))
        + (Math.pow((object1.z - object2.z), 2))
    )
}


function getDistanceRelatively(distance, constantDistance) {
    return distance / constantDistance
}

function calculateDistancesBetweenFingertips(arrayOfFingerPoints) {
    let constantDistance = getDistance(arrayOfFingerPoints[9], arrayOfFingerPoints[13])
    return getDistanceArray(arrayOfFingerPoints, constantDistance)
}

function getDistanceArray(arrayOfFingerPoints, constantDistance) {
    return [
        getDistanceRelatively(
            getDistance(arrayOfFingerPoints[4], arrayOfFingerPoints[8]),
            constantDistance
        ),
        getDistanceRelatively(
            getDistance(arrayOfFingerPoints[8], arrayOfFingerPoints[12]),
            constantDistance
        ),
        getDistanceRelatively(
            getDistance(arrayOfFingerPoints[12], arrayOfFingerPoints[16]),
            constantDistance
        ),
        getDistanceRelatively(
            getDistance(arrayOfFingerPoints[16], arrayOfFingerPoints[20]),
            constantDistance
        ),
        getDistanceRelatively(
            getDistance(arrayOfFingerPoints[20], arrayOfFingerPoints[4]),
            constantDistance
        )
    ];
}

function getAvgDistance(array) {
    return array.reduce(function (r, e, i) {
        e.forEach((a, j) => r[j] = (r[j] || 0) + a)
        if (i == array.length - 1) r = r.map(el => Math.round(el / array.length * 100) / 100);
        return r
    }, [])
}